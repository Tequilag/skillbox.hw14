//
//  SecondTabViewController.swift
//  HW14
//
//  Created by Georgy Gorbenko on 23.08.2020.
//  Copyright © 2020 Letmecode. All rights reserved.
//

import UIKit
import RealmSwift

class SecondTabViewController: TodoViewController {
    
    private let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        navigationItem.title = "Realm"
    }
    
    override func deleteItem(_ item: TodoCellData) {
        
        try? self.realm.write {
            
            self.realm.delete(self.realm.objects(TodoItem.self).filter("id=%@", item.id))
        }
    }
    
    override func createItem(name: String?) {
        
        let item = TodoItem(id: UUID().uuidString, name: name)
        try? self.realm.write {
            
            self.realm.add(item)
        }
    }
    
    override func completeItem(_ item: TodoCellData) {
        
        guard let item = self.realm.objects(TodoItem.self).filter("id=%@", item.id).first else { return }
        
        try? self.realm.write {
            
            item.completed = true
            self.realm.add(item)
        }
    }
    
    override func prepareCells() {
        
        cells = realm.objects(TodoItem.self).map { TodoCellData(id: $0.id, name: $0.name, completed: $0.completed) }
        tableView.reloadData()
    }
    
}
