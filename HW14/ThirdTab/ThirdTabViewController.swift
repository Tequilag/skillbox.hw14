//
//  ThirdTabViewController.swift
//  HW14
//
//  Created by Georgy Gorbenko on 23.08.2020.
//  Copyright © 2020 Letmecode. All rights reserved.
//

import UIKit
import CoreData

class ThirdTabViewController: TodoViewController {
    
    var persistentContainer: NSPersistentContainer = {
          let container = NSPersistentContainer(name: "Todo")
          container.loadPersistentStores(completionHandler: { (storeDescription, error) in
              if let error = error as NSError? {
                  fatalError("Unresolved error \(error), \(error.userInfo)")
              }
          })
          return container
      }()
    
    lazy var context: NSManagedObjectContext = {
        
        return persistentContainer.viewContext
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        navigationItem.title = "CoreData"
    }
    
    override func deleteItem(_ item: TodoCellData) {
        
        let request: NSFetchRequest<TodoItemCD> = TodoItemCD.fetchRequest()
        request.predicate = NSPredicate(format: "id=%@", item.id)
        guard let object = try? context.fetch(request).first else { return }
        context.delete(object)
        saveContext()
    }
    
    override func createItem(name: String?) {
        
        let item = TodoItemCD(context: context)
        item.id = UUID().uuidString
        item.name = name ?? ""
        
        saveContext()
    }
    
    override func completeItem(_ item: TodoCellData) {
        
        let request: NSFetchRequest<TodoItemCD> = TodoItemCD.fetchRequest()
        request.predicate = NSPredicate(format: "id=%@", item.id)
        guard let object = try? context.fetch(request).first else { return }
        object.completed = true
        saveContext()
    }
    
    override func prepareCells() {
        
        let request: NSFetchRequest<TodoItemCD> = TodoItemCD.fetchRequest()
        do {
            
            cells = try context.fetch(request).map { TodoCellData(id: $0.id, name: $0.name, completed: $0.completed) }
        }
        catch {
            
            print(error)
        }
        
        tableView.reloadData()
    }
    
    func saveContext () {
        
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                context.rollback()
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

