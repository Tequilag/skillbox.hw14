//
//  BaseWeatherHeaderView.swift
//  HW14
//
//  Created by Georgy Gorbenko on 16.01.2021.
//

import UIKit

class BaseWeatherHeaderView: UITableViewHeaderFooterView {
    
    private let titleLabel = UILabel()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension BaseWeatherHeaderView {
    
    func fill(weather: Weather) {
        
        titleLabel.text = "\(weather.name ?? ""): \(weather.main.temperature)"
    }
    
}

private extension BaseWeatherHeaderView {
    
    func setupUI() {
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
        
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 28),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -28),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
}
