//
//  BaseWeatherViewController.swift
//  HW14
//
//  Created by Georgy Gorbenko on 16.01.2021.
//

import UIKit

class BaseWeatherViewController: UIViewController {
    
    let tableView = UITableView()
    var weather: Weather?
    var forecast: Forecast?
    
    private let headerIdentifier = String(describing: BaseWeatherHeaderView.self)
    private let cellIdentifier = String(describing: BaseWeatherCell.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func loadCurrentWeather() {
        
    }
    
    func loadForecastWeather() {
        
    }
    
}

// MARK: - UITableViewDataSource
extension BaseWeatherViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return forecast?.list.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BaseWeatherCell,
              let list = forecast?.list else { fatalError() }
        cell.fill(daily: list[indexPath.item])
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let weather = self.weather,
              let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as? BaseWeatherHeaderView else { return nil }
        headerView.fill(weather: weather)
        return headerView
    }
    
}

// MARK: - UITableViewDelegate
extension BaseWeatherViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        guard weather != nil else { return 0 }
        return 44.0
    }
    
}

private extension BaseWeatherViewController {
    
    func setupUI() {
        
        setupTableView()
        loadCurrentWeather()
        loadForecastWeather()
    }
    
    func setupTableView() {
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
        
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        tableView.register(BaseWeatherHeaderView.self, forHeaderFooterViewReuseIdentifier: headerIdentifier)
        tableView.register(BaseWeatherCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorInset.left = 28
        tableView.separatorInset.right = 28
    }
    
}
