//
//  AlamofireLoader.swift
//  HW14
//
//  Created by Georgy Gorbenko on 16.01.2021.
//

import Foundation
import Alamofire

class AlamofireLoader {
    
    func loadCurrentWeather(completion: @escaping ((Weather) -> Void)) {
        
        AF.request("https://api.openweathermap.org/data/2.5/weather",
                   parameters: ["q": "Moscow",
                                "units": "metric",
                                "appid": "6e2fb531c93d6841b4f22b2ba2fcf7e3"])
            .responseData { data in
                
                let decoder = JSONDecoder()
                guard let data = data.data else { return }
                
                do {
                    
                    let weather = try decoder.decode(Weather.self, from: data)
                    DispatchQueue.main.async {
                        
                        completion(weather)
                    }
                }
                catch {
                    
                    print(error)
                }
            }
    }
    
    func loadForecast(completion: @escaping ((Forecast) -> Void)) {
        
        AF.request("https://api.openweathermap.org/data/2.5/onecall",
                   parameters: ["lat": "55.751244",
                                "lon": "37.618423",
                                "units": "metric",
                                "exclude": "current,minutely,hourly,alerts",
                                "appid": "6e2fb531c93d6841b4f22b2ba2fcf7e3"])
            .responseData { data in
                
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .secondsSince1970
                guard let data = data.data else { return }
                
                do {
                    
                    let forecast = try decoder.decode(Forecast.self, from: data)
                    DispatchQueue.main.async {
                        
                        completion(forecast)
                    }
                }
                catch {
                    
                    print(error)
                }
            }
    }
    
}
