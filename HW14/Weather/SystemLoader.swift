//
//  SystemLoader.swift
//  HW14
//
//  Created by Georgy Gorbenko on 16.01.2021.
//

import Foundation

class SystemLoader {
    
    func loadCurrentWeather(completion: @escaping ((Weather) -> Void)) {
        
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?q=Moscow&units=metric&appid=6e2fb531c93d6841b4f22b2ba2fcf7e3") else { return }
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            let decoder = JSONDecoder()
            guard let data = data else { return }
            
            do {
                
                let weather = try decoder.decode(Weather.self, from: data)
                DispatchQueue.main.async {
                    
                    completion(weather)
                }
            }
            catch {
                
                print(error)
            }
        }
        task.resume()
    }
    
    func loadForecast(completion: @escaping ((Forecast) -> Void)) {
        
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/onecall?units=metric&lat=55.751244&lon=37.618423&appid=6e2fb531c93d6841b4f22b2ba2fcf7e3&exclude=current,minutely,hourly,alerts") else { return }
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .secondsSince1970
            guard let data = data else { return }
            
            do {
                
                let forecast = try decoder.decode(Forecast.self, from: data)
                DispatchQueue.main.async {
                    
                    completion(forecast)
                }
            }
            catch {
                
                print(error)
            }
        }
        task.resume()
    }
    
}
