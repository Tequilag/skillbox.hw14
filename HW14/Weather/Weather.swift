//
//  Weather.swift
//  HW14
//
//  Created by Georgy Gorbenko on 16.01.2021.
//

import RealmSwift

class Weather: Object, Decodable {
    
    @objc dynamic var id: String = ""
    @objc dynamic var main: Main!
    @objc dynamic var name: String?
    
    private enum CodingKeys: String, CodingKey {
        
        case main = "main"
        case name = "name"
    }
    
    required init(from decoder: Decoder) throws {
    
        let container = try decoder.container(keyedBy: CodingKeys.self)
        main = try container.decode(Main.self, forKey: .main)
        name = try? container.decode(String.self, forKey: .name)
    }
    
    required init() {
    }
    
}

class Main: Object, Decodable {
    
    @objc dynamic var temperature: Double = 0
    
    private enum CodingKeys: String, CodingKey {
        
        case temperature = "temp"
    }
    
    required init(from decoder: Decoder) throws {
    
        let container = try decoder.container(keyedBy: CodingKeys.self)
        temperature = try container.decode(Double.self, forKey: .temperature)
    }
    
    required init() {
    }
}
