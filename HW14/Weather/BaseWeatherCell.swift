//
//  BaseWeatherCell.swift
//  HW14
//
//  Created by Georgy Gorbenko on 16.01.2021.
//

import UIKit

class BaseWeatherCell: UITableViewCell {
    
    private let dateLabel = UILabel()
    private let dayLabel = UILabel()
    private let minLabel = UILabel()
    private let maxLabel = UILabel()
    private let nightLabel = UILabel()
    private let eveLabel = UILabel()
    private let mornLabel = UILabel()
    private let dateFormatter = DateFormatter()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension BaseWeatherCell {
    
    func fill(daily: Daily) {
        
        dateLabel.text = dateFormatter.string(from: daily.date)
        dayLabel.text = "Day:\n\(daily.temperature.day)"
        minLabel.text = "Min:\n\(daily.temperature.min)"
        maxLabel.text = "Max:\n\(daily.temperature.max)"
        nightLabel.text = "Night:\n\(daily.temperature.night)"
        eveLabel.text = "Eve:\n\(daily.temperature.eve)"
        mornLabel.text = "Morn:\n\(daily.temperature.morn)"
    }
    
}

private extension BaseWeatherCell {
    
    func setupUI() {
        
        dateFormatter.dateFormat = "yy-MM-dd"
        
        let contentStackView = UIStackView()
        contentStackView.spacing = 6
        contentStackView.axis = .vertical
        
        dateLabel.textAlignment = .left
        
        
        for label in [dayLabel, minLabel, maxLabel, nightLabel, eveLabel, mornLabel] {
            
            label.numberOfLines = 3
            label.textAlignment = .left
        }
        
        let infoStackView = UIStackView()
        infoStackView.axis = .horizontal
        infoStackView.alignment = .center
        infoStackView.spacing = 4
        
        contentStackView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(contentStackView)
        
        NSLayoutConstraint.activate([
        
            contentStackView.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 12),
            contentStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 28),
            contentStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -28),
            contentStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -12)
        ])
        
        contentStackView.addArrangedSubview(dateLabel)
        contentStackView.addArrangedSubview(infoStackView)
        infoStackView.addArrangedSubview(dayLabel)
        infoStackView.addArrangedSubview(minLabel)
        infoStackView.addArrangedSubview(maxLabel)
        infoStackView.addArrangedSubview(nightLabel)
        infoStackView.addArrangedSubview(eveLabel)
        infoStackView.addArrangedSubview(mornLabel)
    }
    
}
