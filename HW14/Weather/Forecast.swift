//
//  Forecast.swift
//  HW14
//
//  Created by Georgy Gorbenko on 16.01.2021.
//

import RealmSwift

class Forecast: Object, Decodable {
    
    let list = List<Daily>()
    
    private enum CodingKeys: String, CodingKey {
        
        case list = "daily"
    }
    
    required init(from decoder: Decoder) throws {
    
        let container = try decoder.container(keyedBy: CodingKeys.self)
        list.append(objectsIn: try container.decode([Daily].self, forKey: .list))
    }
    
    required init() {
    }
    
}

class Daily: Object, Decodable {
    
    @objc dynamic var date: Date = Date()
    @objc dynamic var temperature: Temperature!
    
    private enum CodingKeys: String, CodingKey {
        
        case date = "dt"
        case temperature = "temp"
    }
    
    required init(from decoder: Decoder) throws {
    
        let container = try decoder.container(keyedBy: CodingKeys.self)
        date = try container.decode(Date.self, forKey: .date)
        temperature = try container.decode(Temperature.self, forKey: .temperature)
    }
    
    required init() {
    }
    
}

class Temperature: Object, Decodable {
    
    @objc dynamic var day: Double = 0
    @objc dynamic var min: Double = 0
    @objc dynamic var max: Double = 0
    @objc dynamic var night: Double = 0
    @objc dynamic var eve: Double = 0
    @objc dynamic var morn: Double = 0
    
    private enum CodingKeys: String, CodingKey {
        
        case day = "day"
        case min = "min"
        case max = "max"
        case night = "night"
        case eve = "eve"
        case morn = "morn"
    }
    
    required init(from decoder: Decoder) throws {
    
        let container = try decoder.container(keyedBy: CodingKeys.self)
        day = try container.decode(Double.self, forKey: .day)
        min = try container.decode(Double.self, forKey: .min)
        max = try container.decode(Double.self, forKey: .max)
        night = try container.decode(Double.self, forKey: .night)
        eve = try container.decode(Double.self, forKey: .eve)
        morn = try container.decode(Double.self, forKey: .morn)
    }
    
    required init() {
    }
    
}
