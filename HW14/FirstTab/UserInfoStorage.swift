//
//  UserInfoStorage.swift
//  HW14
//
//  Created by Georgy Gorbenko on 17.01.2021.
//

import Foundation

class UserInfoStorage {
    
    static let shared = UserInfoStorage()
    
    private let defaults = UserDefaults.standard
    private let kFirstNameKey = "UserInfoStorage.FirstName"
    private let kLastNameKey = "UserInfoStorage.LastName"
    
    var firstName: String? {
        
        set {
            
            defaults.set(newValue, forKey: kFirstNameKey)
        }
        get {
            
            return defaults.string(forKey: kFirstNameKey)
        }
        
    }
    
    var lastName: String? {
        
        set {
            
            defaults.set(newValue, forKey: kLastNameKey)
        }
        get {
            
            return defaults.string(forKey: kLastNameKey)
        }
        
    }
    
    private init() {}
}
