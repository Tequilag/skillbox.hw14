//
//  FirstTabViewController.swift
//  HW14
//
//  Created by Georgy Gorbenko on 23.08.2020.
//  Copyright © 2020 Letmecode. All rights reserved.
//

import UIKit

class FirstTabViewController: UIViewController {
    
    @IBOutlet private weak var firstNameTextField: UITextField!
    @IBOutlet private weak var lastNameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstNameTextField.text = UserInfoStorage.shared.firstName
        lastNameTextField.text = UserInfoStorage.shared.lastName
    }
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        
        switch sender {
        case firstNameTextField:
            UserInfoStorage.shared.firstName = sender.text
        case lastNameTextField:
            UserInfoStorage.shared.lastName = sender.text
        default:
            break
        }
    }
    
}
