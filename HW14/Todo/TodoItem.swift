//
//  TodoItem.swift
//  HW14
//
//  Created by Georgy Gorbenko on 17.01.2021.
//

import RealmSwift

class TodoItem: Object {
    
    override class func primaryKey() -> String? { return "id" }
    
    @objc dynamic var id: String = ""
    @objc dynamic var name: String?
    @objc dynamic var completed: Bool = false
    
    convenience init(id: String, name: String?) {
        self.init()
        
        self.id = id
        self.name = name
    }
    
}
