//
//  TodoItemCD.swift
//  HW14
//
//  Created by Georgy Gorbenko on 17.01.2021.
//

import Foundation
import CoreData

@objc(TodoItemCD)
public class TodoItemCD: NSManagedObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TodoItemCD> {
        return NSFetchRequest<TodoItemCD>(entityName: "TodoItemCD")
    }

    @NSManaged public var id: String
    @NSManaged public var name: String?
    @NSManaged public var completed: Bool
}
