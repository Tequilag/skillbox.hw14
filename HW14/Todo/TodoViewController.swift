//
//  TodoViewController.swift
//  HW14
//
//  Created by Georgy Gorbenko on 17.01.2021.
//

import UIKit
import RealmSwift

class TodoViewController: UIViewController {
    
    let tableView = UITableView()
    var cells = [TodoCellData]()
    
    private let cellIdentifier = String(describing: TodoCell.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupUI()
    }
    
    func deleteItem(_ item: TodoCellData) {
        
    }
    
    func createItem(name: String?) {
        
    }
    
    func prepareCells() {
        
        
    }
    
    func completeItem(_ item: TodoCellData) {
        
    }
    
}

// MARK: - UITableViewDataSource
extension TodoViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TodoCell else { fatalError() }
        cell.fill(data: cells[indexPath.item])
        return cell
    }
    
}

// MARK: - UITableViewDelegate
extension TodoViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let alertController = UIAlertController(title: cells[indexPath.item].name, message: nil, preferredStyle: .actionSheet)
        let completeAction = UIAlertAction(title: "Complete", style: .default) { _ in
            
            self.completeItem(self.cells[indexPath.item])
            self.prepareCells()
        }
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { _ in
            
            self.deleteItem(self.cells[indexPath.item])
            self.prepareCells()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alertController.addAction(cancelAction)
        alertController.addAction(deleteAction)
        alertController.addAction(completeAction)
        present(alertController, animated: true)
    }
    
}

private extension TodoViewController {
    
    func setupUI() {
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addButtoDidTap))]
        
        setupTableView()
        prepareCells()
    }
    
    func setupTableView() {
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
        
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])

        tableView.register(TodoCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorInset.left = 28
        tableView.separatorInset.right = 28
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    @objc func addButtoDidTap() {
        
        let alertController = UIAlertController(title: "Create todo item", message: nil, preferredStyle: .alert)
        alertController.addTextField { textField in
            
            textField.placeholder = "Enter name"
        }
        let saveAction = UIAlertAction(title: "Save", style: .default) { _ in
            
            self.createItem(name: alertController.textFields?[0].text)
            self.prepareCells()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        present(alertController, animated: true)
    }
    
}
