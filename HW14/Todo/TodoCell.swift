//
//  TodoCell.swift
//  HW14
//
//  Created by Georgy Gorbenko on 17.01.2021.
//

import UIKit

class TodoCell: UITableViewCell {
    
    private let nameLabel = UILabel()
    private let completedView = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
}

extension TodoCell {
    
    func fill(data: TodoCellData) {
        
        nameLabel.text = data.name
        completedView.backgroundColor = !data.completed ? .clear : .green
        completedView.layer.borderColor = data.completed ? UIColor.clear.cgColor : UIColor.black.cgColor
    }
    
}

private extension TodoCell {
    
    func setupUI() {
        
        nameLabel.textAlignment = .left
        nameLabel.numberOfLines = 0
        
        let contentStackView = UIStackView()
        contentStackView.alignment = .center
        contentStackView.axis = .horizontal
        contentStackView.distribution = .equalSpacing
        
        contentStackView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(contentStackView)
        
        NSLayoutConstraint.activate([
        
            contentStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12),
            contentStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 28),
            contentStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -28),
            contentStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -12),
            completedView.widthAnchor.constraint(equalToConstant: 16),
            completedView.heightAnchor.constraint(equalToConstant: 16)
        ])
        
        completedView.layer.borderWidth = 1
        completedView.layer.cornerRadius = 8
        contentStackView.addArrangedSubview(nameLabel)
        contentStackView.addArrangedSubview(completedView)
    }
    
}
