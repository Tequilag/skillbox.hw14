//
//  TodoCellData.swift
//  HW14
//
//  Created by Georgy Gorbenko on 17.01.2021.
//

import Foundation

struct TodoCellData {
    
    let id: String
    let name: String?
    let completed: Bool
}
