//
//  FourthTabViewController.swift
//  HW14
//
//  Created by Georgy Gorbenko on 17.01.2021.
//

import UIKit
import RealmSwift

class FourthTabViewController: BaseWeatherViewController {
    
    private let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Weather"
        prepareData()
    }
    
    override func loadCurrentWeather() {
    
        AlamofireLoader().loadCurrentWeather { [weak self] weather in
            
            guard let self = self else { return }
            try? self.realm.write {
                
                self.realm.delete(self.realm.objects(Weather.self))
                self.realm.add(weather)
            }
            self.weather = weather
            self.tableView.reloadData()
        }
    }
    
    override func loadForecastWeather() {

        AlamofireLoader().loadForecast { [weak self] forecast in

            guard let self = self else { return }
            try? self.realm.write {
                
                self.realm.delete(self.realm.objects(Forecast.self))
                self.realm.add(forecast)
            }
            self.forecast = forecast
            self.tableView.reloadData()
        }
    }
    
    func prepareData() {
        
        weather = realm.objects(Weather.self).first
        forecast = realm.objects(Forecast.self).first
    }
    
}
